import 'package:equatable/equatable.dart';
import 'system_fields.dart';

abstract class Entry<T> extends Equatable {
  final T fields;
  final SystemFields sys;

  Entry({
    this.sys,
    this.fields,
  });

  @override
  List<Object> get props => [sys, fields];
}

class EntryCollection<T extends Entry> {
  final int total;
  final int skip;
  final int limit;
  final List<T> items;

  EntryCollection({
    this.total,
    this.skip,
    this.limit,
    this.items,
  });

  factory EntryCollection.fromJson(
    Map<String, dynamic> json,
    T Function(Map<String, dynamic>) fromJson,
  ) =>
      EntryCollection(
        total: json['total'],
        skip: json['skip'],
        limit: json['limit'],
        items: List<T>.from(
          json['items']?.map((item) => fromJson(item)) ?? [],
        ),
      );
}
