import 'dart:io';

import 'package:http/http.dart' as http;

class HttpClient extends http.BaseClient {
  final http.Client _client;
  final String _accessToken;

  HttpClient._internal(this._client, this._accessToken);

  factory HttpClient(String token) {
    final client = http.Client();
    return HttpClient._internal(client, token);
  }

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers[HttpHeaders.authorizationHeader] = 'Bearer $_accessToken';

    return _client.send(request);
  }
}
