class Includes {
  final IncludesMap map;

  Includes(this.map);

  factory Includes.fromJson(Map<String, dynamic> json) {
    final map = IncludesMap.fromJson(json);

    return Includes(map);
  }

  bool _isLink(Map<String, dynamic> entry) {
    return entry['sys'] != null && entry['sys']['type'] == 'Link';
  }

  bool _isListOfLinks(List<dynamic> list) {
    if (list.isEmpty) {
      return false;
    }

    if (!(list.first is Map)) {
      return false;
    }

    return _isLink(list.first);
  }

  Map<String, dynamic> _walkMap(Map<String, dynamic> entry) {
    if (_isLink(entry)) {
      final resolved = map.resolveLink(entry);
      return _isLink(resolved) ? entry : _walkMap(resolved);
    } else if (entry['fields'] == null) return entry;

    final fields = entry['fields'] as Map<String, dynamic>;

    entry['fields'] = fields.map((key, fieldJson) {
      if (fieldJson is List && _isListOfLinks(fieldJson)) {
        return MapEntry<String, dynamic>(
          key,
          resolveLinks(fieldJson),
        );
      } else if (fieldJson is Map && _isLink(fieldJson)) {
        return MapEntry<String, dynamic>(
          key,
          _walkMap(map.resolveLink(fieldJson)),
        );
      }
      return MapEntry<String, dynamic>(key, fieldJson);
    });
    return entry;
  }

  List<Map<String, dynamic>> resolveLinks(List<dynamic> items) {
    return items.map((item) => _walkMap(item)).toList();
  }
}

class IncludesMap {
  IncludesMap(this._map);

  factory IncludesMap.fromJson(Map<String, dynamic> json) {
    final Map<String, Map<String, Map<String, dynamic>>> map = {};

    json.forEach((type, json) {
      if (map[type] == null) map[type] = {};
      final list = json as List<dynamic>;
      list.forEach((json) {
        final entry = json as Map<String, dynamic>;
        map[type][entry['sys']['id']] = entry;
      });
    });

    return IncludesMap(map);
  }

  final Map<String, Map<String, Map<String, dynamic>>> _map;

  Map<String, dynamic> resolveLink(Map<String, dynamic> link) {
    final String type = link['sys']['linkType'];
    final String id = link['sys']['id'];
    if (_map[type] == null || _map[type][id] == null) return link;
    return _map[type][id];
  }
}
