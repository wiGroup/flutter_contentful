import 'dart:convert';

import 'package:flutter_contentful/http_client.dart';

import 'includes.dart';
import 'models/entry.dart';

class ContentfulClient {
  final String host;
  final String _spaceId;
  final String _environment;
  final HttpClient _client;

  static ContentfulClient _instance;
  static ContentfulClient get instance => _instance;

  ContentfulClient._internal(
    this._client,
    this._spaceId,
    this._environment, {
    this.host,
  });

  factory ContentfulClient(
    spaceId,
    accessToken,
    environment, {
    host = 'cdn.contentful.com',
  }) {
    final client = HttpClient(accessToken);
    _instance =
        ContentfulClient._internal(client, spaceId, environment, host: host);

    return _instance;
  }

  Uri _uri(String path, {Map<String, String> params}) => Uri.https(
        host,
        '/spaces/$_spaceId/environments/$_environment$path',
        params,
      );

  Future<T> getEntry<T extends Entry>(
    String id,
    T Function(Map<String, dynamic>) fromJson, {
    Map<String, String> params,
  }) async {
    final response = await _client.get(_uri('/entries/$id', params: params));

    if (response.statusCode != 200) {
      throw Exception('getEntry failed');
    }

    return fromJson(json.decode(utf8.decode(response.bodyBytes)));
  }

  Future<EntryCollection<T>> getEntries<T extends Entry>(
    Map<String, String> query,
    T Function(Map<String, dynamic>) fromJson,
  ) async {
    final response = await _client.get(_uri('/entries', params: query));

    if (response.statusCode != 200) {
      throw Exception('getEntries failed');
    }

    dynamic jsonr = json.decode(utf8.decode(response.bodyBytes));

    if (jsonr['includes'] != null) {
      final includes = Includes.fromJson(jsonr['includes']);
      jsonr['items'] = includes.resolveLinks(jsonr['items']);
    }

    return EntryCollection.fromJson(jsonr, fromJson);
  }
}
