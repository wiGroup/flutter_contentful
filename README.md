# flutter_contentful

Contentful Delivery Api.

## Getting Started

This project use json annotation
To generate your model or when model has changed please run - 

```flutter packages pub run build_runner build```

### How to use 
- initialise the plugin first

```
ContentfulClient(
    'SPACE_ID',
    'ACCESS_TOKEN',
    'ENVIRONMENT',
  );
```

- Create your model 

```
class InAppOffer extends Entry<InAppOfferFields> {
  static final CONTENT_TYPE = 'spotInAppOffer';

  InAppOffer({
    SystemFields sys,
    InAppOfferFields fields,
  }) : super(sys: sys, fields: fields);

  factory InAppOffer.fromJson(Map<String, dynamic> json) => InAppOffer(
        sys: SystemFields.fromJson(json['sys']),
        fields: InAppOfferFields.fromJson(json['fields']),
      );
}

class InAppOfferFields {
  final String offerName;
  final Asset offerCardImage;
  final String offerUniqueReference;

  InAppOfferFields({
    this.offerName,
    this.offerCardImage,
    this.offerUniqueReference,
  });

  factory InAppOfferFields.fromJson(Map<String, dynamic> json) =>
      InAppOfferFields(
        offerName: json['offerName'],
        offerCardImage: Asset.fromJson(json['offerCardImage']),
        offerUniqueReference: json['offerUniqueReference'],
      );
}
```

- Use contenful instance to fetch the contents

```
ContentfulClient.instance.getEntries<InAppOffer>(
      {'content_type': InAppOffer.CONTENT_TYPE},
      (json) => InAppOffer.fromJson(json),
    ).then((values) {
      print(values);
    });
```